from math import sqrt
from Algorithms.solvability_service import SolvabilityService


class SlidingPuzzle:
    def __init__(self, parent_app, board=None, labelNumMov=None, label_score=None, loaded=None):
        """
        Constructor del juego. Puede cargar una partida o inicializar una nueva. Configura los listener del teclado.

        :param parent_app: Application que llama a la clase
        :param board: Board con el que resolvera el juego
        :param labelNumMov: Etiqueta del número de movimientos
        :param label_score: Etiqueta con el marcador
        :param loaded: Juego que se desea cargar
        """
        self.board = board
        self.labelNumMov = labelNumMov
        self.labelScore = label_score
        self.parent_app = parent_app
        self.score = 0

        # Se inicializa los tiles con la informacion del board
        self.tiles = board.init_tiles()
        self.size_row = int(sqrt(self.tiles.get_size_of_tiles()))

        if loaded is None:
            # Comienza un juego nuevo
            self.keysPressed = []
            self.tiles.shuffle()
            self.ss = SolvabilityService(self.board.grid, self.tiles.get_numbers_from_tiles(), "")

            while not self.ss.is_solvable():
                self.tiles.shuffle()
                self.ss.set_new_list_equal_dimension(self.tiles.get_numbers_from_tiles(), "")

            self.init_list = self.tiles.get_numbers_from_tiles()
        else:
            # Carga el juego pasado por el parametro board
            print(loaded)
            self.tiles.load(loaded['init_positions'])
            self.keysPressed = loaded['moves'][:]
            self.init_list = loaded['init_positions']
            for move in loaded['moves']:
                self.move_gap(move)

        # Se cargan las tiles con su posicion actual y sus listeners
        self.tiles.update_tiles()
        self.update_binds()

        # Se configuran los listeners
        self.board.focus_set()
        self.board.bind("<Key-Up>", self.key)
        self.board.bind("<Key-Down>", self.key)
        self.board.bind("<Key-Left>", self.key)
        self.board.bind("<Key-Right>", self.key)

    def key(self, event):
        """
        Funcion a la que se llama cuando se pulsa una telca.

        :param event: Evento de la tecla pulsada
        :return: None
        """
        # Se mueve el gap según el evento
        self.move_gap(event.keysym)

    def move_gap(self, event):
        """
        Mueve el gap según el evento que se haya generado.

        :param event: Dirección hacia la que se mueve el gap.
        :return: None
        """

        # Se obtienen los posibles movimientos y los tiles hacia los que se puede mover el gap
        up, down, left, right = self.tiles.get_tiles_arround_gap()

        # Se mueve el gap y se actualiza el marcador
        # Gap hacia arriba
        if event == "Up":
            if self.tiles.swap_tile_with_gap(up):   # Es posible el movimiento
                self.keysPressed.append("Up")
                self.score += 1
                print("Up")
            else:
                self.score += 5                     # No es posible el movimiento

        # Gap hacia abajo
        elif event == "Down":
            if self.tiles.swap_tile_with_gap(down):
                self.keysPressed.append("Down")     # Es posible el movimiento
                self.score += 1
                print("Down")
            else:
                self.score += 5                     # No es posible el movimiento

        elif event == "Left":
            if self.tiles.swap_tile_with_gap(left):
                self.keysPressed.append("Left")     # Es posible el movimiento
                self.score += 1
                print("Left")
            else:
                self.score += 5                     # No es posible el movimiento

        elif event == "Right":
            if self.tiles.swap_tile_with_gap(right):
                self.keysPressed.append("Right")    # Es posible el movimiento
                self.score += 1
                print("Right")
            else:
                self.score += 5                     # No es posible el movimiento

        # Se deshabilita el listener del ratón para los tiles anteriores al movimiento
        for tile in (up, down, left, right):
            if tile is not None:
                tile.unbind("<Button-1>")

        # Se actualizan las etiquteas del marcador y movimientos de la GUI
        self.labelNumMov.configure(text=str(len(self.keysPressed)))
        self.labelScore.configure(text=str(self.score))

        # Se comprueba si se ha finalizado el juego
        if self.tiles.update_tiles():
            self.end_game()

        # Se actualizan los listeners del teclado
        self.update_binds()

    def end_game(self):
        """
        Función que se llama al finalizar el juego y que avisa a la GUI

        :return: None
        """
        self.parent_app.end_game()

    def update_binds(self):
        """
        Se acutalizan los listener del teclado de los botones "arriba" "abajo" "izquierda" "derecha"

        :return: None
        """
        # Se obtienen los tiles que se deben de añadir el listener
        up, down, left, right = self.tiles.get_tiles_arround_gap()

        # Tile de arriba
        if up is not None:
            up.bind("<Button-1>", lambda e: self.move_gap("Up"))

        # Tile de abajo
        if down is not None:
            down.bind("<Button-1>", lambda e: self.move_gap("Down"))

        # Tile de izquierda
        if left is not None:
            left.bind("<Button-1>", lambda e: self.move_gap("Left"))

        # Tile de derecha
        if right is not None:
            right.bind("<Button-1>", lambda e: self.move_gap("Right"))

    def save(self):
        """
        Devuelve el juego guardado en un diccionario

        :return: Diccionario con los detalles del juego a guardar
        """
        return{'dim': self.size_row,
                'init_positions': self.init_list,
                'moves': self.keysPressed}

    def unbind(self):
        """
        Deshabilita las teclas del teclado

        :return: None
        """
        self.board.unbind("<Key-Up>")
        self.board.unbind("<Key-Down>")
        self.board.unbind("<Key-Left>")
        self.board.unbind("<Key-Right>")

