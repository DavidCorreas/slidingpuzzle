import tkinter as tk

from Classes.tile import Tile
from Classes.tiles import Tiles


class Board(tk.Frame):
    """
    Clase Board: frame modificado que contiene a los tiles
    """
    def __init__(self, parent, grid):
        """
        Constructor.
        :param parent: Donde se va a colocar
        :param grid: dimension
        """
        tk.Frame.__init__(self, parent)
        self.grid = grid
        self.parent = parent
        self.configure_board()

    def configure_board(self):
        """
        Funcion que configura el contenido del grid de board.
        :return:
        """
        for it in range(self.grid):
            self.rowconfigure(it, weight=1)
            self.columnconfigure(it, weight=1)

    def init_tiles(self):
        """
        Funcion que crea los tiles y los pone dentro del board
        :return: un objeto de la clase Tiles
        """
        print("CREATING TILES...")
        tiles = Tiles(self.grid)
        number = 1
        for row in range(self.grid):
            for column in range(self.grid):
                if row == column == self.grid - 1:
                    tile = Tile(self, (row, column), "")
                    tiles.add_tile(tile)
                    tiles.set_gap_tile(tile)
                else:
                    tiles.add_tile(Tile(self, (row, column), str(number)))
                number += 1
        return tiles
