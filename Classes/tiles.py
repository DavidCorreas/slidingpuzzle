import random as rnd


class Tiles:
    def __init__(self, grid):
        """
        Constructor de Tiles

        :param grid: Grid que contiene a las tiles
        """
        self.grid = grid
        self.tiles = []
        self.gapTile = None

    def get_size_of_tiles(self):
        """
        Obtiene el número de tiles

        :return: Numero de tiles
        """
        return len(self.tiles)

    def add_tile(self, tile):
        """
        Añade la tile al conjunto

        :param tile: Tile a añadir
        :return: None
        """
        print("Created: " + tile.__str__())
        self.tiles.append(tile)

    def get_numbers_from_tiles(self):
        """
        Devuelve la lista que representa las posiciones de los tiles

        :return: Lista del board actual
        """
        alist = []
        for element in self.tiles:
            if element.number != "":
                alist.append(int(element.number))
            else:
                alist.append(element.number)
        return alist

    def set_gap_tile(self, tile):
        """
        Añade el gap al conjunto

        :param tile: Tile del gap
        :return: None
        """
        self.gapTile = tile

    def get_gap_tile(self):
        """
        Obtiene el tile del gap

        :return: Tile del gap
        """
        return self.gapTile

    def update_tiles(self):
        """
        Actualiza la posición actual de los tiles dentro del board

        :return: True si se ha completado el juego
        """
        terminado = True
        for tile in self.tiles:
            tile.update()
            if not tile.in_position():
                terminado = False
        return terminado

    def get_tile_position(self, *position):
        """
        Devuelve el tile de la posición determinada

        :param position: Posición en la que buscar el tile
        :return: Tile que se encuentra en la posicion especifiada
        """
        for tile in self.tiles:
            if tile.get_current_position() == position:
                return tile

    def get_tile_number(self, number):
        """
        Obteien el tile segun su numero

        :param number: Numero del tile que se quiere obtener
        :return: Tile con el numero actualizado
        """
        for tile in self.tiles:
            if tile.get_number() == number:
                return tile

    def shuffle(self):
        """
        Desoredena aleatoriamente los tiles del juego

        :return: None
        """
        print("SHUFFLING...")
        # Funcion que ordena los tiles de manear aleatoria
        self.tiles = sorted(self.tiles, key=lambda x: rnd.random())
        it = 0
        for row in range(self.grid):
            for col in range(self.grid):
                self.tiles[it].set_current_position((row, col))
                print("Shuffled: " + self.tiles[it].__str__())
                it += 1

    def load(self, loaded_list):
        """
        Carga la disposición especificada en la lista dada

        :param loaded_list: Lista que especifica el orden de los tiles dentro del board
        :return: None
        """
        it = 0
        for row in range(self.grid):
            for col in range(self.grid):
                self.get_tile_number(str(loaded_list[it])).set_current_position((row, col))
                it += 1

    def get_tiles_arround_gap(self):
        """
        Obtiene los tiles que se encuentran al rededor del gap

        :return: Tupla de los cuatro tiles "up" "down" "left" "right"
        """
        row, col = self.gapTile.get_current_position()
        return (self.get_tile_position(row - 1, col), self.get_tile_position(row + 1, col),
                self.get_tile_position(row, col - 1), self.get_tile_position(row, col + 1))

    def swap_tile_with_gap(self, tile):
        """
        Cambia las posiciones del gap y del tile dado

        :param tile: Tile con el que se tiene que cambiar el gap de posicion
        :return: True si el movimiento es valido, False si no lo es.
        """
        try:
            position_gap = self.gapTile.get_current_position()
            position_tile = tile.get_current_position()
            self.gapTile.set_current_position(position_tile)
            tile.set_current_position(position_gap)
            return True
        except AttributeError:
            # Movimiento invalido
            print("Invalid Movement: You cant move in that direction")
            return False
