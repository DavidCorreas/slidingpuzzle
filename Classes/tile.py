import tkinter as tk


class Tile(tk.Label):

    fontTile = "Segoe UI Bold"

    def __str__(self):
        """
        Imprime el tile por pantalla

        :return: String con el numero del tile y su posición
        """
        return "Tile with Number: " + self.number + " at: " + str(self.currentPosition)

    def set_position(self, position):
        """
        Configura la posicion final

        :param position: Posicion final
        :return: None
        """
        self.position = position

    def get_position(self):
        """
        Obtiene la posición final

        :return: Posicion final
        """
        return self.position

    def set_current_position(self, position):
        """
        Configura la posicion actual

        :param position: Posicion a la que mover
        :return: None
        """
        self.currentPosition = position

    def get_current_position(self):
        """
        Obtiene la posicion actual

        :return: Posicion actual
        """
        return self.currentPosition

    def get_final_position(self):
        """
        Obtiene la posicion final

        :return: Posicicion final
        """
        return self.position

    def get_number(self):
        """
        Obtiene el numero del tile

        :return: Numero del tile
        """
        return self.number

    def in_position(self):
        """
        Pregunta si el tile está en su posición final

        :return: True si está colocada, False si no esta colocada
        """
        return self.currentPosition == self.position

    def __init__(self, parent, position, number):
        """
        Constructor del tile

        :param parent: Tk frame padre
        :param position: Posicion final
        :param number: Numero del tile
        """
        tk.Label.__init__(self, parent)
        self.position = position
        self.currentPosition = position
        self.number = number

        self.configure_tile()

    def update(self):
        """
        Actualiza la poisición del tile dentro del tablero

        :return:
        """
        self.grid(row=self.currentPosition[0], column=self.currentPosition[1])

    def configure_tile(self):
        """
        Configura el aspecto del tile

        :return: None
        """
        self.grid(sticky="nsew")
        if self.number != "":
            # No es el gap
            self.configure(background="#26467E", fg="#ffffff")
            self.configure(text=self.number, font=(self.fontTile, 16))
            self.configure(borderwidth=2, relief=tk.RAISED)
            # Hoover del tile
            self.bind("<Enter>", lambda e: self.configure(bg="#cecece"))
            self.bind("<Leave>", lambda e: self.configure(bg="#26467E"))
        else:
            # Es el gap
            self.configure(background="#181818")
            self.configure(text=" ", font=(self.fontTile, 16))
            self.configure(borderwidth=2, relief=tk.SUNKEN)
