import threading


class SolvabilityService:
    """
    Clase encargada de la generacion de puzzles con solucion
    optimizada con el uso de varios hilos de ejecucion.
    """
    def __init__(self, dimension, alist, gap):
        """
        Contructor del servicio.
        :param dimension: dimension
        :param alist: lista del tablero
        :param gap: gap
        """
        self.dimension = dimension
        self.alist = alist
        self.inversions = 0
        self.rowOfEmptyElement = 0

        self.gap = gap
        self.workersAnsInversions = [0]*(dimension * dimension)
        self.workersAnsFindX = [False]*dimension

        self.init()

    def init(self):
        for index in range(self.dimension * self.dimension):
            if self.alist[index] != self.gap:
                threading.Thread(target=self.worker_inversions, args=(index,)).start()
            if index % self.dimension == 0:
                threading.Thread(target=self.worker_findx, args=(index,)).start()

        self.inversions = sum(self.workersAnsInversions)
        self.rowOfEmptyElement = self.dimension - self.workersAnsFindX.index(True)

    def worker_inversions(self, index):
        """
        Hilo que calcula las inversiones de cada celda.
        :param index: indice de la lista
        :return: None
        """
        inversionscounter = 0
        for element in self.alist[index:]:
            if element != self.gap:
                if self.alist[index] > element:
                    inversionscounter += 1
        self.workersAnsInversions[index] = inversionscounter

    def worker_findx(self, index):
        """
        Hilo que se encarga de buscar el gap
        :param index: indice de la lista
        :return: None
        """
        for element in self.alist[index:index + self.dimension]:
            if element == self.gap:
                self.workersAnsFindX[index // self.dimension] = True
                break

    def is_solvable(self):
        """
        Funcion comprueba la solvabilidad de un puzle
        :return: true si el puzzle tiene solucion
        """
        dimensionpar = self.dimension % 2 == 0
        inversionpar = self.inversions % 2 == 0
        rowofemptypar = self.rowOfEmptyElement % 2 == 0
        # Case 1
        if not dimensionpar and inversionpar:
            return True
        # Case 2:
        if dimensionpar and rowofemptypar and not inversionpar:
            return True
        # Case 3:
        if dimensionpar and not rowofemptypar and inversionpar:
            return True
        return False

    def set_new_list(self, dimension, anotherlist, gap):
        """
        Funcion que permite cambiar la lista que representa el puzzle
        con dimension diferente.
        :param dimension: dimension
        :param anotherlist: nueva lista
        :param gap: gap
        :return: None
        """
        self.dimension = dimension
        self.set_new_list_equal_dimension(anotherlist, gap)

    def set_new_list_equal_dimension(self, anotherlist, gap):
        """
        Funcion que permite cambiar la lista que representa el puzzle
        de la misma dimension.
        :param anotherlist: nueva lista
        :param gap: gap
        :return: None
        """
        self.alist = anotherlist
        self.gap = gap
        self.workersAnsInversions = [0] * (self.dimension * self.dimension)
        self.workersAnsFindX = [False] * self.dimension
        self.init()


#
# Main para probar la clase SolvabilityService
#
if __name__ == "__main__":
    print("==================================================================")
    print("TEST: 1")
    print("==================================================================")

    p1list = [1, 8, 2, 0, 4, 3, 7, 6, 5]
    ss = SolvabilityService(3, p1list, 0)

    print("Dimension: " + str(ss.dimension) + "         // should be: 3")
    print("Lista" + str(ss.alist) + "           // should be: [1, 8, 2, 0, 4, 3, 7, 6, 5]")
    print("Emptychar:" + str(ss.gap) + "         // should be: 0")

    print("Answers from workers getting inversions: " + str(ss.workersAnsInversions))
    print("Anwsers from workers getting the row " + str(ss.workersAnsFindX))

    print("Number or inversions: " + str(ss.inversions) + "         // should be: 10")
    print("Row counting from the bottom: " + str(ss.rowOfEmptyElement) + "          // should be: 2")

    print("Solvable?: " + str(ss.is_solvable()) + "           // should be: True")

    print("==================================================================")
    print("TEST: 2")
    print("==================================================================")

    p2list = [1, 8, 2, "x", 4, 3, 7, 6, 5]
    ss.set_new_list_equal_dimension(p2list, "x")

    print("Dimension: " + str(ss.dimension) + "         // should be: 3")
    print("Lista" + str(ss.alist) + "           // should be: [1, 8, 2, x, 4, 3, 7, 6, 5]")
    print("Emptychar:" + str(ss.gap) + "         // should be: x")

    print("Answers from workers getting inversions: " + str(ss.workersAnsInversions))
    print("Anwsers from workers getting the row " + str(ss.workersAnsFindX))

    print("Number or inversions: " + str(ss.inversions) + "         // should be: 10")
    print("Row counting from the bottom: " + str(ss.rowOfEmptyElement) + "          // should be: 2")

    print("Solvable?: " + str(ss.is_solvable()) + "           // should be: True")

    print("==================================================================")
    print("TEST: 3")
    print("==================================================================")

    p3list = [6, 13, 7, 10, 8, 9, 11, 0, 15, 2, 12, 5, 14, 3, 1, 4]
    ss.set_new_list(4, p3list, 0)

    print("Dimension: " + str(ss.dimension) + "         // should be: 4")
    print("Lista" + str(ss.alist) + "           // should be: [6, 13, 7, 10, 8, 9, 11, 0, 15, 2, 12, 5, 14, 3, 1, 4]")
    print("Emptychar:" + str(ss.gap) + "         // should be: 0")

    print("Answers from workers getting inversions: " + str(ss.workersAnsInversions))
    print("Anwsers from workers getting the row " + str(ss.workersAnsFindX))

    print("Number or inversions: " + str(ss.inversions) + "         // should be: 62")
    print("Row counting from the bottom: " + str(ss.rowOfEmptyElement) + "          // should be: 3")

    print("Solvable?: " + str(ss.is_solvable()) + "           // should be: True")

    print("==================================================================")
    print("TEST: 4")
    print("==================================================================")

    p4list = [3, 9, 1, 15, 14, 11, 4, 6, 13, 0, 10, 12, 2, 7, 8, 5]
    ss.set_new_list_equal_dimension(p4list, 0)

    print("Dimension: " + str(ss.dimension) + "         // should be: 4")
    print("Lista" + str(ss.alist) + "           // should be: [3, 9, 1, 15, 14, 11, 4, 6, 13, 0, 10, 12, 2, 7, 8, 5]")
    print("Emptychar:" + str(ss.gap) + "         // should be: 0")

    print("Answers from workers getting inversions: " + str(ss.workersAnsInversions))
    print("Anwsers from workers getting the row " + str(ss.workersAnsFindX))

    print("Number or inversions: " + str(ss.inversions) + "         // should be: 56")
    print("Row counting from the bottom: " + str(ss.rowOfEmptyElement) + "          // should be: 2")

    print("Solvable?: " + str(ss.is_solvable()) + "           // should be: False")

    print("==================================================================")
    print("TEST: 5")
    print("==================================================================")

    p5list = [13, 2, 10, 3, 1, 12, 8, 4, 5, "", 9, 6, 15, 14, 11, 7]
    ss.set_new_list_equal_dimension(p5list, "")

    print("Dimension: " + str(ss.dimension) + "         // should be: 4")
    print("Lista" + str(ss.alist) + "           // should be: [13, 2, 10, 3, 1, 12, 8, 4, 5, "", 9, 6, 15, 14, 11, 7]")
    print("Emptychar:" + str(ss.gap) + "         // should be: """)

    print("Answers from workers getting inversions: " + str(ss.workersAnsInversions))
    print("Anwsers from workers getting the row " + str(ss.workersAnsFindX))

    print("Number or inversions: " + str(ss.inversions) + "         // should be: 41")
    print("Row counting from the bottom: " + str(ss.rowOfEmptyElement) + "          // should be: 2")

    print("Solvable?: " + str(ss.is_solvable()) + "           // should be: True")

    print("==================================================================")
    print("TEST: 6")
    print("==================================================================")

    p6list = [1, 2, 3, 4, 5, 6, 7, "", 8]
    ss1 = SolvabilityService(3, p6list, "")

    print("Dimension: " + str(ss1.dimension))
    print("Lista" + str(ss1.alist))
    print("Emptychar:" + str(ss1.gap))

    print("Answers from workers getting inversions: " + str(ss1.workersAnsInversions))
    print("Anwsers from workers getting the row " + str(ss1.workersAnsFindX))

    print("Number or inversions: " + str(ss1.inversions))
    print("Row counting from the bottom: " + str(ss1.rowOfEmptyElement))

    print("Solvable?: " + str(ss1.is_solvable()))
