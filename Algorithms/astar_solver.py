import math
from random import random

from Algorithms.solvability_service import SolvabilityService


class Node:
    """
    Clase Node: Representacion de un estado de resolucion del puzzle
    """
    def __init__(self, parent, board, nivel, gap):
        """
        Constructor
        :param parent: Padre
        :param board: Tablero
        :param nivel: nivel de profundidad del arbol
        :param gap: gap
        """
        self.parent = parent
        self.board = board
        self.nivel = nivel
        self.gap = gap
        self.lenboard = len(self.board)
        self.dimension = int(math.sqrt(self.lenboard))
        self.indgap = self.board.index(self.gap)
        self.h = 0
        self.g = 0
        if nivel > 0:
            self.find_h()
            self.find_g()
        self.f = self.g + self.h

    def __eq__(self, other):
        return self.board == other.board

    def move(self, place):
        """
        Funcion que aplica un movimiento al nodo
        :param place: direccion a mover
        :return: el nuevo tablero
        """
        _board = self.board[:]
        if place == "up":
            _toswap = self.indgap - self.dimension
        elif place == "down":
            _toswap = self.indgap + self.dimension
        elif place == "right":
            _toswap = self.indgap + 1
        else:
            _toswap = self.indgap - 1

        aux = _board[_toswap]
        _board[_toswap] = self.gap
        _board[self.indgap] = aux
        return _board

    def get_childrens(self):
        """
        Funcion que devuelve los hijos de un nodo
        :return: hijos de un nodo.
        """
        childrens = []
        if not self.indgap < self.dimension:
            childrens.append(Node(self, self.move("up"), self.nivel + 1, self.gap))
        if not self.indgap >= self.lenboard - self.dimension:
            childrens.append(Node(self, self.move("down"), self.nivel + 1, self.gap))
        if not self.indgap % self.dimension == 0:
            childrens.append(Node(self, self.move("left"), self.nivel + 1, self.gap))
        if not self.indgap % self.dimension == self.dimension - 1:
            childrens.append(Node(self, self.move("right"), self.nivel + 1, self.gap))
        return childrens

    def __str__(self):
        """
        Imprime el nodo. Solo usada para debug.
        :return: None
        """
        if self.nivel > 0:
            print(str(self.board)+" NIVEL: " + str(self.nivel)
                  + ", PARENT NIVEL: " + str(self.parent.nivel) + ".")
        elif self.nivel == 0:
            print(str(self.board) + " NIVEL: " + str(self.nivel)
                  + ", NO PARENT.")
        else:
            print(str(self.board) + " NODO OBJETIVO.")

    def find_g(self):
        """
        Funcion que calcula la G de la heuristica
        en este caso se tiene en cuenta la g del padre y se le suma 1
        en indicando mayor profundidad.
        :return: g
        """
        self.g = self.parent.g + 1

    def find_h(self):
        """
        Funcion que calcula la H de la heuristica
        en este caso se hace uso de la distancia de Manhattan
        tanto de cada ficha a su posicion original como de la distancia
        de un puzzle en un estado semi-resuelto:
        - en caso de tener la primera fila [ 1,  2,  3 ] estaremos
        potenciado que se expanda ese puzzle en los hijos.
        - en caso de tener la primera columna resueslta
        [ 1
        [ 4
        [ 7
        se potencia que explore estas soluciones.
        :return: h
        """
        i = 0
        for iter1 in range(1, self.dimension+1):  # busco los elemntos en la primera fila.
            ind_actual = self.board.index(iter1)
            self.h += abs(ind_actual - i)
            i += 1
        i = 0
        for iter2 in range(self.dimension+1, len(self.board), self.dimension):
            ind_actual = self.board.index(iter2)
            self.h += abs(ind_actual - i*self.dimension)
            i += 1
        i = 0
        for iter3 in range(1, self.lenboard):
            ind_actual = self.board.index(iter3)
            self.h += abs(ind_actual - i)
            i += 1


class AstarSolver:
    """
    Clase AstarSolver: IA que resuelve el puzzle mediante heuristica
    """

    def __init__(self, startnode, endnode, gap):
        """
        Constructor
        :param startnode: tablero inicial
        :param endnode: tablero final
        :param gap: tipo de gap
        """
        self.startnode = Node(None, startnode, 0, gap)
        self.endnode = Node(None, endnode, -1, gap)
        self.nodos_abiertos = []
        self.nodos_cerrados = []
        self.ss = SolvabilityService(self.startnode.dimension, self.startnode.board, gap)

    def solve(self):
        """
        Funcion que resuelve el nodo actual.
        :return: Lista de nodos hasta la solucion
        """

        if not self.ss.is_solvable():
            return "No solucion"

        self.nodos_abiertos.append(self.startnode)
        self.startnode.__str__()

        while len(self.nodos_abiertos) > 0:
            # Ordeno la lista de Nodos Abiertos:
            self.nodos_abiertos.sort(key=lambda node: node.f)

            # Sacamos de la lista el nodo que vamos a analizar de la lista de Nodos Abiertos
            # Siempre sacaremos el mejor nodo dado que se ordena la lista.
            nodo_actual = self.nodos_abiertos.pop(0)

            # Comparamos el nodo actual con el nodo objetivo si concuerdan salimos del programa
            # devolviendo la lista de los nodos por lo que se paso.
            # De esta forma se devuelve la PRIMERA solucion encontrada.
            if nodo_actual == self.endnode:
                solucion = []
                current = nodo_actual
                while current is not None:
                    solucion.append(current.board)
                    current = current.parent
                return solucion[::-1]

            # Comprobamos que los hijos no esten en las listas de Nodos Abiertos o Cerrados.
            childrens = nodo_actual.get_childrens()
            for child in childrens:
                if not self.procesado(child):
                    self.nodos_abiertos.append(child)

            # Terminamos de analizar el nodo actual por lo que lo mandamos a la lista de nodos cerrados.
            self.nodos_cerrados.append(nodo_actual)
        # Volvemos para analizar los nodos que estan en Nodos Abiertos.

    def procesado(self, child):
        """
        :param child: nodo a revisar
        :return: true si el nodo ha sido procesado.
        """
        if child in self.nodos_abiertos:
            return True
        if child in self.nodos_cerrados:
            return True
        return False


def genera_lista(dim, gap):
    """
    Funcion que genera un tablero con solucion y su solucion
    devueltos ambos en una tupla.
    :param dim: dimension del tablero a genrerar
    :param gap: gap
    :return: tupla con la lista generada y la lista objetivo
    """
    alist = []
    generated = []

    for i in range(dim * dim):
        if i == dim * dim - 1:
            alist.append(gap)
        else:
            alist.append(i+1)

    listgenerated = sorted(alist, key=lambda x: random())

    ss = SolvabilityService(dim, listgenerated, gap)
    while not ss.is_solvable():
        listgenerated = sorted(alist, key=lambda x: random())
        ss.set_new_list_equal_dimension(listgenerated, gap)

    generated.append(listgenerated)
    generated.append(alist)

    return generated


#
# Main usado para probar el AstarSolver
#
if __name__ == "__main__":

    x2x2 = [1, 2, 3, "x"]
    x3x3 = [1, 2, 3, 4, 5, 6, 7, 8, "x"]
    x4x4 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, "x"]

    list2x2 = [2, 3, 1, "x"]    # 2x2 tablero
    list3x3 = [1, 8, 2, "x", 4, 3, 7, 6, 5]     # 3x3 tablero lo resuelve
    list3x32 = ["x", 2, 4, 1, 8, 7, 3, 6, 5]    # no solucion
    list3x33 = [1, 2, 3, 4, 5, 6, "x", 7, 8]
    list4x4 = [6, 13, 7, 10, 8, 9, 11, "x", 15, 2, 12, 5, 14, 3, 1, 4]     # 4x4 tablero
    list4x4notSol = [3, 9, 1, 15, 14, 11, 4, 6, 13, "x", 10, 12, 2, 7, 8, 5]    # no solucion

    N = 3
    lista = genera_lista(N, "x")

    listaC = [1, 4, 5, 6, 3, 2, 8, 'x', 7]
    # path = AstarSolver(listaC, x3x3).solve()
    print(lista[1])
    path = AstarSolver(lista[0], lista[1], "x").solve()
    print("\nResultado: " + str(path))
    print("Movimientos totales: " + str(len(path)-1))
