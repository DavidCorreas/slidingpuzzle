from GUI import application


def init(top, gui):
    """
    Funcion que inicializa la ventana
    :param top:
    :param gui:
    :return:
    """
    global w, top_level, root
    w = gui
    top_level = top
    root = top


def destroy_window():
    """
    Funcion que cierra la ventana
    :return: None
    """
    global top_level
    top_level.destroy()
    top_level = None


if __name__ == '__main__':
    application.start_gui()
