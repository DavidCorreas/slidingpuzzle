import threading
import random
import numpy as np

from collections import deque
from keras.models import Sequential
from keras.models import load_model
from keras.layers.core import Dense, Activation
from keras.optimizers import Adam, SGD
from keras.layers.normalization import BatchNormalization


class SolvabilityService:  # Todo: hacer bien la gestion de los hilos.
    
    def __init__(self, dimension, alist, emptychar):
        self.dimension = dimension
        self.alist = alist
        self.inversions = 0
        self.rowOfEmptyElement = 0

        self.emptychar = emptychar
        self.workersAnsInversions = [0]*(dimension * dimension)
        self.workersAnsFindX = [False]*dimension

        self.init()

    def init(self):
        for index in range(self.dimension * self.dimension):
            if self.alist[index] != self.emptychar:
                threading.Thread(target=self.worker_inversions, args=(index,)).start()
                #  self.worker_inversions(index)
            if index % self.dimension == 0:
                threading.Thread(target=self.worker_findx, args=(index,)).start()
                #  self.worker_findx(index)

        self.inversions = sum(self.workersAnsInversions)
        self.rowOfEmptyElement = self.dimension - self.workersAnsFindX.index(True)

    def worker_inversions(self, index):
        inversionscounter = 0
        for element in self.alist[index:]:
            if element != self.emptychar:
                if self.alist[index] > element:
                    inversionscounter += 1
        self.workersAnsInversions[index] = inversionscounter

    def worker_findx(self, index):
        for element in self.alist[index:index + self.dimension]:
            if element == self.emptychar:
                self.workersAnsFindX[index // self.dimension] = True
                break

    def is_solvable(self):
        dimensionpar = self.dimension % 2 == 0
        inversionpar = self.inversions % 2 == 0
        rowofemptypar = self.rowOfEmptyElement % 2 == 0
        # Case 1
        if not dimensionpar and inversionpar:
            return True
        # Case 2:
        if dimensionpar and rowofemptypar and not inversionpar:
            return True
        # Case 3:
        if dimensionpar and not rowofemptypar and inversionpar:
            return True
        return False

    def set_new_list(self, dimension, anotherlist, emptychar):
        self.dimension = dimension
        self.set_new_list_equal_dimension(anotherlist, emptychar)

    def set_new_list_equal_dimension(self, anotherlist, emptychar):
        self.alist = anotherlist
        self.emptychar = emptychar
        self.workersAnsInversions = [0] * (self.dimension * self.dimension)
        self.workersAnsFindX = [False] * self.dimension
        self.init()


def board_generator(dim, gap):
    alist = []
    generated = []

    for i in range(dim * dim):
        if i == dim * dim - 1:
            alist.append(gap)
        else:
            alist.append(i+1)

    listgenerated = alist[:]
    random.shuffle(listgenerated)
    
    ss = SolvabilityService(dim, listgenerated, gap)
    while not ss.is_solvable():
        random.shuffle(listgenerated)
        ss.set_new_list_equal_dimension(listgenerated, gap)

    generated.append(listgenerated)
    generated.append(alist)

    return generated


class Tile:
    def __init__(self, row, col, num, dim):
        self.row = row
        self.col = col
        self.num = num
        self.dim = dim
        self.state = Tile.calculate_status(self)

    @staticmethod
    def calculate_status(self):
        alist = [0] * self.dim ** 2
        alist[self.col + self.dim * self.row] = 1
        return alist

    def __str__(self):
        return str("TILE NUMBER " + str(self.num) + "\n(ROW " + str(self.row) 
                   + ", COLUMN " + str(self.col) + ")" +
                   "\nDIMENSION " + str(self.dim) + "   >>> status_list " 
                   + str(self.state))


class Tiles:
    
    reward_good_mov = -1
    reward_bad_mov = -5
    
    def __init__(self, status_list, gap):
        self.dim = int(np.sqrt(len(status_list)))
        self.gap = gap
        self.status_list = status_list
        self.state = sorted(Tiles.__getstatusfrom(status_list, self.dim), key=lambda tle: tle.num)
        self.end = board_generator(self.dim, self.gap)[1]

    def render(self, list):
        
        if list is None:
            list = self.status_list

        for i in range(self.dim):
            print("\n", end="")
            for x in list[i * self.dim:i * self.dim + self.dim]:
                if x != self.gap:
                    print(x, " ", end="")
                else:
                    print("\u258A", " ", end="")
        print("\n")

    def next_state(self, action):
        if action == "up":
            the_next_state = Tiles.__move_up(self.status_list, self.status_list.index(self.gap), self.gap)
        elif action == "left":
            the_next_state = Tiles.__move_left(self.status_list, self.status_list.index(self.gap), self.gap)
        elif action == "down":
            the_next_state = Tiles.__move_down(self.status_list, self.status_list.index(self.gap), self.gap)
        elif action == "right":
            the_next_state = Tiles.__move_right(self.status_list, self.status_list.index(self.gap), self.gap)
        else:
            raise Exception(EnvironmentError)

        if the_next_state[0].status_list == the_next_state[0].end:
            the_next_state.append(True)
        else:
            the_next_state.append(False)
        
        return the_next_state

    @staticmethod
    def __getrowof(numero, status_list, dim):
        for row in range(dim):
            status_frag = status_list[row * dim:row * dim + dim]
            if numero in status_frag:
                return row

    @staticmethod
    def __getcolumnof(numero, status_list, dim):
        column = status_list.index(numero) % dim
        return column

    @staticmethod
    def __getstatusfrom(status_list, dim):
        status = []
        for numero in status_list:
            row = Tiles.__getrowof(numero, status_list, dim)
            col = Tiles.__getcolumnof(numero, status_list, dim)
            status.append(Tile(row, col, numero, dim))
        return status

    @staticmethod
    def __move_up(status_list, indgap, gap):
        # (next_state, reward, done)
        _dim = int(np.sqrt(len(status_list)))
        if indgap < _dim:
            # Si es menor que la dimension quiere decir que esta en la primera
            # parte de la lista o lo que es lo mismo, que está en la parte
            # de arriba del tablero.
            return [Tiles(status_list, gap), Tiles.reward_bad_mov]
        else:
            _next_state = status_list[:]
            # Calculo a donde tiene que ir el gap
            toswap = indgap - _dim
            # Intercambio por burbuja
            aux = _next_state[toswap]
            _next_state[toswap] = gap
            _next_state[indgap] = aux
            return [Tiles(_next_state, gap), Tiles.reward_good_mov]

    @staticmethod
    def __move_left(status_list, indgap, gap):
        _dim = int(np.sqrt(len(status_list)))
        if indgap % _dim == 0:
            # Si el indice del gap es 0 mod dim quiere decir que está
            # en la primera columna.
            return [Tiles(status_list, gap), Tiles.reward_bad_mov]
        else:
            _next_state = status_list[:]
            # Calculo a donde tiene que ir el gap
            toswap = indgap - 1
            # Intercambio por burbuja
            aux = _next_state[toswap]
            _next_state[toswap] = gap
            _next_state[indgap] = aux
            return [Tiles(_next_state, gap), Tiles.reward_good_mov]

    @staticmethod
    def __move_down(status_list, indgap, gap):
        lenboard = len(status_list)
        _dim = int(np.sqrt(lenboard))
        if indgap >= lenboard - _dim:
            # Si es mayor o igual que la long - dim quiere decir que el gap
            # se encuentra en la parte final de la lista, por lo tanto la
            # ultima columna.
            return [Tiles(status_list, gap), Tiles.reward_bad_mov]
        else:
            _next_state = status_list[:]
            # Calculo a donde tiene que ir el gap
            toswap = indgap + _dim
            # Intercambio por burbuja
            aux = _next_state[toswap]
            _next_state[toswap] = gap
            _next_state[indgap] = aux
            return [Tiles(_next_state, gap), Tiles.reward_good_mov]

    @staticmethod
    def __move_right(status_list, indgap, gap):
        _dim = int(np.sqrt(len(status_list)))
        if indgap % _dim == _dim - 1:
            # Si el indice del gap modulo dim es la dim-1 quiere decir que
            # está en la última columna.
            return [Tiles(status_list, gap), Tiles.reward_bad_mov]
        else:
            _next_state = status_list[:]
            # Calculo a donde tiene que ir el gap
            toswap = indgap + 1
            # Intercambio por burbuja
            aux = _next_state[toswap]
            _next_state[toswap] = gap
            _next_state[indgap] = aux
            return [Tiles(_next_state, gap), Tiles.reward_good_mov]


class NPuzzleEnviroment:
    up = [1, 0, 0, 0]
    right = [0, 1, 0, 0]
    down = [0, 0, 1, 0]
    left = [0, 0, 0, 1]

    def __init__(self, dimension=3):
        # Set-up enviroment
        self.dimension = dimension
        self.gap = dimension * 10
        self.observation_space = dimension ** 2
        self.action_space = 4
        self.possible_actions = [NPuzzleEnviroment.up,
                                 NPuzzleEnviroment.right,
                                 NPuzzleEnviroment.down,
                                 NPuzzleEnviroment.left]
                                 
        self.game = Tiles(board_generator(self.dimension, self.gap)[0], self.gap)
        self.best_state = self.game.status_list

    def get_state(self):
        return [tile.state for tile in self.game.state]

    def render(self, best=False):
        if best:
            self.game.render(self.best_state)
        else:
            self.game.render(None)

    def make_action(self, action):
        if action == NPuzzleEnviroment.up:
            self.game, reward, done = self.game.next_state("up")
        elif action == NPuzzleEnviroment.right:
            self.game, reward, done = self.game.next_state("right")
        elif action == NPuzzleEnviroment.down:
            self.game, reward, done = self.game.next_state("down")
        elif action == NPuzzleEnviroment.left:
            self.game, reward, done = self.game.next_state("left")
        else:
            raise Exception(EnvironmentError)
            
        ss_self_best_state = SolvabilityService(self.dimension, self.best_state, self.gap)
        ss_pos_best_state = SolvabilityService(self.dimension, self.game.status_list, self.gap)
        
        if ss_self_best_state.inversions > ss_pos_best_state.inversions:
            self.best_state = self.game.status_list
            reward = 10

        return reward, done

    def get_inversions(self):
        ss = SolvabilityService(self.dimension, self.game.status_list, self.gap)
        return ss.inversions

    def get_best_state_inversions(self):
        ss = SolvabilityService(self.dimension, self.best_state, self.gap)
        return ss.inversions


class DQNAgent:
    def __init__(self, state_size, action_size):
        self.state_size = state_size
        self.action_size = action_size
        self.memory = deque(maxlen=500)
        self.gamma = 0.95    # discount rate
        self.epsilon = 1.0  # exploration rate
        self.epsilon_min = 0.01
        self.epsilon_decay = 0.995
        self.learning_rate = 0.1
        self.model = self._build_network()
        self.historic = []
        self.best_memory = deque(maxlen=500)

    def _build_network(self):
        model = Sequential()
        model.add(Dense(81, input_dim=self.state_size))
        #model.add(BatchNormalization())
        model.add(Activation('relu'))

        model.add(Dense(250))
        #model.add(BatchNormalization())
        model.add(Activation('relu'))

        model.add(Dense(250))
        #model.add(BatchNormalization())
        model.add(Activation('relu'))

        # model.add(Dense(30))
        # model.add(BatchNormalization())
        # model.add(Activation('softmax'))

        model.add(Dense(self.action_size))
        # model.add(BatchNormalization())
        model.add(Activation('linear'))

        model.compile(loss='mse', optimizer=SGD(lr=self.learning_rate, decay=1e-4, momentum=0.9, nesterov=True))

        return model
    
    def remember(self, state, action, reward, next_state, done, best=False):
        if best:
            self.best_memory.append((state, action, reward, next_state, done))
        else:
            self.memory.append((state, action, reward, next_state, done))
    
    
    def act(self, state):
        if np.random.rand() <= self.epsilon:
            return random.randrange(self.action_size)
        act_values = self.model.predict(state)
        return np.argmax(act_values[0])  # returns action
    
    def replay(self, batch_size):
        
        total_memory =  list(self.memory + self.best_memory)
        random.shuffle(total_memory)
        
        minibatch = random.sample(total_memory, batch_size)
        
        for state, action, reward, next_state, done in minibatch:
            target = reward
           
            if not done:
                target = reward + self.gamma * np.amax(self.model.predict(next_state)[0])

            target_f = self.model.predict(state)

            target_f[0][action] = target
       
            # target_f_01 = np.where(target_f == target, 1, 0)

            history = self.model.fit(state, target_f, epochs=0, verbose=0)
            self.historic.append(history)

        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay
    
    def save_model(self, path_model):
        self.model.save(path_model)

    def load_model(self, path_model):
        self.model = load_model(path_model)


# ENTRENAMIENTO
agent = DQNAgent(81, 4)
episodes = 2000
max_steps = 200

for episode in range(episodes):
  
    print("==================================================================================")
    print("EPISODE: {} / {}".format(episode, episodes))
    print("==================================================================================")
    
    env = NPuzzleEnviroment()
    state = np.array(env.get_state(), "float32")
    state = np.reshape(state, [1, 81])
  
    episode_rewards = []

    for step in range(max_steps):

        if episode % 10 == 0:
            env.render()

        action = agent.act(state)

        reward, done = env.make_action(env.possible_actions[action])
        next_state_inversions = env.get_inversions()

        next_state = np.array(env.get_state(), "float32")
        next_state = np.reshape(next_state, [1, 81])
    
        if done:
            reward = 100
            agent.remember(state, action, reward, next_state, done)
            step = max_steps
            
            print("RESUELTO")
        else:
            if reward == 10:    
                agent.remember(state, action, reward, next_state, done, best=True)
            else:
                agent.remember(state, action, reward, next_state, done, best=False)
        
        episode_rewards.append(reward)

    print("BEST STATE EPISODE: ", env.get_best_state_inversions())
    env.render(best=True)
    print("REWARD: {}".format(np.sum(episode_rewards)))
    print("==================================================================================")
    
    agent.replay(128)

    path_model = 'model_3x3_sgd.keras'
    agent.save_model(path_model)
    

"""
path_model = 'model_3x3.keras'
trained_agent = DQNAgent(81, 4)
trained_agent.load_model(path_model)

env = NPuzzleEnviroment()
done = False
i = 0
while not done:
    print(i) 
    env.render()
    state = np.array(env.get_state())
    state = np.reshape(state, [1,81])
    action = trained_agent.act(state)
    print(action)
    reward, done = env.make_action(env.possible_actions[action])
    i += 1
    if reward == -2:
        break;

print("Resuelto en i: ", i)"""
