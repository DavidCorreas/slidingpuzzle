import os
import pickle
import time
import tkinter as tk
from collections import defaultdict
from tkinter import simpledialog

from Classes.board import Board
from Game.sliding_puzzle import SlidingPuzzle
from Algorithms.astar_solver import AstarSolver

import init_application


def start_gui():
    """
    Boot the application.

    :return: None
    """
    global val, w, global_root
    global_root = tk.Tk()
    top = Application(global_root)
    init_application.init(global_root, top)
    global_root.mainloop()


w = None


def create_application(root):
    """
    This create the aplication with de Tk frame given.

    :param root: The root frame of the GUI
    :return: la ventana princiapl
    """
    global w, rt
    rt = root
    w = tk.Toplevel(root)
    top = Application(w)
    init_application.init(w, top)
    return w, top


def destroy_application():
    """
    This destroy the application safely

    :return: None
    """
    global w
    w.destroy()
    w = None


class Application(tk.Frame):
    """
    This class handle all games and the GUI.
    """

    # This two values handle the min and max size of the board that a player can choose
    _combo_min_value = 2
    _combo_max_value = 9

    # This are data structures for storing the unfinished games and win games
    _unfinished_games = {}
    _game = None
    _win_games = defaultdict(list)
    _store_dict = {"unfinished": {}, "win": defaultdict(list)}
    _load_window = None
    _refresh_time = 0.5

    # Path for the storing file
    path = './games_saved.obj'

    # Default color for the GUI
    _colorPrimary = "#00469f"
    _colorAccent = "#001232"
    _colorAux = "#cecece"
    _containerColor = "#8fff53"  # Debug color for develop the frame

    # Fonts for the GUI
    _fontLight = "Segoe UI Light"
    _fontBold = "Segoe UI Bold"
    _fontPrimaryColor = "#5b5b5b"

    _comboValues = list(range(_combo_min_value, _combo_max_value))

    def __init__(self, parent=None):
        """
        Constructor for the class Application
        :param parent: Root TK Frame
        """
        tk.Frame.__init__(self, parent)

        self.grid = tk.IntVar()
        self.grid.set(3)

        # Top is the root window.
        self.parent = parent
        self.parent.geometry("1000x650")
        self.parent.bind("<Configure>", lambda e: self.board_update())
        self.parent.title("Sliding Puzzle")
        self.parent.protocol("WM_DELETE_WINDOW", self.on_closing)

        # Application ATTRIBUTES
        self.frameMenuLeft = tk.Frame(self.parent)
        self.containerFile = tk.Frame(self.frameMenuLeft)
        self.btnExit = tk.Button(self.containerFile)
        self.btnLoad = tk.Button(self.containerFile)
        self.btnSave = tk.Button(self.containerFile)
        self.btnGiveUp = tk.Button(self.containerFile)
        self.containerRight = tk.Frame(self.parent)
        self.labelTitle = tk.Label(self.containerRight)
        self.frameMenuTop = tk.Frame(self.containerRight)
        self.frameMain = tk.Frame(self.containerRight)
        self.containerBoard = tk.Frame(self.frameMain)

        self.containerGameStats = tk.Frame(self.containerBoard)
        self.board = tk.Frame(self.containerBoard)

        self.labelPuntos = tk.Label(self.containerGameStats)
        self.labelNumPuntos = tk.Label(self.containerGameStats)
        self.labelMovements = tk.Label(self.containerGameStats)
        self.labelNumMov = tk.Label(self.containerGameStats)
        self.opMenu = tk.OptionMenu(self.containerGameStats, self.grid, *self._comboValues)
        self.btnStart = tk.Button(self.containerGameStats)
        self.btnIA = tk.Button(self.containerGameStats)

        # Launch Config Elements
        self.config_elements()
        self.search_games()

    def config_elements(self):
        """
        Configure all the elements of the GUI

        :return: None
        """
        # frameMenuLeft Config.
        self.frameMenuLeft.pack(side=tk.LEFT, fill=tk.Y)
        self.frameMenuLeft.configure(background=self._colorAccent)
        self.frameMenuLeft.configure(width=100)

        # rightContainer
        self.containerRight.pack(side=tk.LEFT, fill=tk.BOTH, expand=True)
        self.containerRight.configure(background=self._containerColor)

        # FileContainer
        self.containerFile.place(x=0, y=107, width=107, height=428)
        self.containerFile.configure(background=self._colorPrimary)

        # labelTitle Config.
        self.labelTitle.pack(fill=tk.X)
        self.labelTitle.configure(background="white", foreground=self._fontPrimaryColor)
        self.labelTitle.configure(text='''Sliding Puzzle''', font=(self._fontLight, 24))
        self.labelTitle.configure(anchor=tk.W, relief=tk.FLAT, borderwidth=30)

        # frameMenuTop Config
        self.frameMenuTop.pack(fill=tk.X)
        self.frameMenuTop.configure(height=35)
        self.frameMenuTop.configure(background=self._colorAux)

        # frameMain Config
        self.frameMain.pack(fill=tk.BOTH, expand=True)
        self.frameMain.configure(background=self._colorPrimary)

        # boardContainer Config
        self.containerBoard.pack(fill=tk.BOTH, expand=True, side=tk.LEFT)
        self.containerBoard.configure(background=self._colorPrimary)

        # boardContainer Config
        self.containerGameStats.pack(fill=tk.X, side=tk.TOP)
        self.containerGameStats.configure(background=self._colorPrimary)

        # labelMovements Config
        self.labelMovements.pack(ipady=5, side=tk.LEFT)
        self.labelMovements.configure(background=self._colorPrimary, foreground="white")
        self.labelMovements.configure(text='''Movements:''', font=(self._fontLight, 18))
        self.labelMovements.configure(anchor=tk.W, relief=tk.FLAT, borderwidth=20)

        # labelNumMov Config
        self.labelNumMov.pack(ipady=5, side=tk.LEFT)
        self.labelNumMov.configure(background=self._colorPrimary, foreground="white")
        self.labelNumMov.configure(text='''0''', font=(self._fontLight, 18))
        self.labelNumMov.configure(anchor=tk.W, relief=tk.FLAT, borderwidth=20)

        # labelMovements Config
        self.labelPuntos.pack(ipady=5, side=tk.LEFT)
        self.labelPuntos.configure(background=self._colorPrimary, foreground="white")
        self.labelPuntos.configure(text='''Score: ''', font=(self._fontLight, 18))
        self.labelPuntos.configure(anchor=tk.W, relief=tk.FLAT, borderwidth=20)

        # labelNumMov Config
        self.labelNumPuntos.pack(ipady=5, side=tk.LEFT)
        self.labelNumPuntos.configure(background=self._colorPrimary, foreground="white")
        self.labelNumPuntos.configure(text='''0''', font=(self._fontLight, 18))
        self.labelNumPuntos.configure(anchor=tk.W, relief=tk.FLAT, borderwidth=20)

        # btnStart Config
        self.btnStart.pack(side=tk.RIGHT, padx=10)
        self.btnStart.configure(text="START", font=(self._fontLight, 18))
        self.btnStart.configure(background="white", foreground=self._fontPrimaryColor)
        self.btnStart.configure(command=self.start_game)

        # opMenu Config
        self.opMenu.pack(side=tk.RIGHT, padx=10)

        # btnIA Config
        self.btnIA.configure(text="IA", font=(self._fontLight, 18))
        self.btnIA.configure(background="white", foreground=self._fontPrimaryColor)
        self.btnIA.configure(command=self.solve)
        self.btnIA.configure(width=self.btnIA.winfo_height()*4)

        # btnLoad Config
        self.btnLoad.place(x=0, y=0, width=107, height=107)
        self.btnLoad.configure(text="Load", font=(self._fontLight, 14))
        self.btnLoad.configure(background=self._colorAccent, foreground="white")
        self.btnLoad.configure(relief=tk.FLAT)
        self.btnLoad.configure(command=self.load)
        self.btnLoad.bind("<Enter>", lambda e: self.btnLoad.configure(bg=self._colorPrimary))
        self.btnLoad.bind("<Leave>", lambda e: self.btnLoad.configure(bg=self._colorAccent))

        # btnSave Config
        self.btnSave.place(x=0, y=107, width=107, height=107)
        self.btnSave.configure(text="Save", font=(self._fontLight, 14))
        self.btnSave.configure(background=self._colorAccent, foreground="white")
        self.btnSave.configure(command=self.save)
        self.btnSave.configure(relief=tk.FLAT)
        self.btnSave.bind("<Enter>", lambda e: self.btnSave.configure(bg=self._colorPrimary))
        self.btnSave.bind("<Leave>", lambda e: self.btnSave.configure(bg=self._colorAccent))

        # btnGiveUp Config
        self.btnGiveUp.place(x=0, y=214, width=107, height=107)
        self.btnGiveUp.configure(text="Give Up", font=(self._fontLight, 14))
        self.btnGiveUp.configure(background=self._colorAccent, foreground="white")
        self.btnGiveUp.configure(relief=tk.FLAT)
        self.btnGiveUp.configure(command=self.give_up)
        self.btnGiveUp.bind("<Enter>", lambda e: self.btnGiveUp.configure(bg=self._colorPrimary))
        self.btnGiveUp.bind("<Leave>", lambda e: self.btnGiveUp.configure(bg=self._colorAccent))

        # btnExit Config
        self.btnExit.place(x=0, y=321, width=107, height=107)
        self.btnExit.configure(text="Exit", font=(self._fontLight, 14))
        self.btnExit.configure(background=self._colorAccent, foreground="white")
        self.btnExit.configure(relief=tk.FLAT)
        self.btnExit.configure(command=self.exit)
        self.btnExit.bind("<Enter>", lambda e: self.btnExit.configure(bg=self._colorPrimary))
        self.btnExit.bind("<Leave>", lambda e: self.btnExit.configure(bg=self._colorAccent))

    def start_game(self, status=None):
        """
        Start a new game
        :param status: game previusly stored
        :return: None
        """
        self.board.pack_forget()
        self.btnIA.pack(side=tk.LEFT, padx=10)

        # Create and configure the board of the new game
        if status is None:
            self.board = Board(self.containerBoard, self.grid.get())
        else:
            self.board = Board(self.containerBoard, status.get('dim'))
        self.board.pack(fill=tk.Y, expand=True, padx=25, pady=25, ipadx=5, ipady=5)
        self.board.configure(bg=self._colorPrimary)

        # Initialices the labels
        self.labelNumMov.configure(text=str(0))
        self.labelNumPuntos.configure(text=str(0))

        # Create a new game
        self._game = SlidingPuzzle(self, self.board, self.labelNumMov, self.labelNumPuntos, status)

    def end_game(self):
        """
        Finish the game safely and stores the result

        :return: None
        """

        # Disable all the keys and buttons
        self._game.unbind()

        # Stores the result of the score with the name of the player
        winner_name = simpledialog.askstring("Input",
                                             "Congratulations!\n You won this time... But, how should we call you?",
                                             parent=self.parent)

        self._win_games[self._game.size_row].append({"name": winner_name, "score": self._game.score})

        file = open(self.path, 'wb')
        pickle.dump(self._store_dict, file)
        file.close()

        # Show the scores of the other players
        self._win_games[self._game.size_row].sort(key=lambda x: x["score"])

        ranking_str = "NAME:\t\t SCORE:"
        for d in self._win_games[self._game.size_row]:
            ranking_str = ranking_str + "\n" + str(d["name"]) + "\t\t" + str(d["score"])

        title = str(self._game.size_row) + "x" + str(self._game.size_row) + " winners"
        tk.messagebox.showinfo(title, ranking_str)

        self._game = None

    def save(self):
        """
        Save an unfinished game

        :return: None
        """
        if self._game is not None:
            # Ask for teh name of the game
            game_name = simpledialog.askstring("Input", "How do you want to name this game?", parent=self.parent)
            self._unfinished_games[game_name] = self._game.save()
            self._store_dict["unfinished"] = self._unfinished_games

            # Store the game
            file = open(self.path, 'wb')
            pickle.dump(self._store_dict, file)
            file.close()

    def exit(self):
        """
        Close the application safely

        :return: None
        """
        self.on_closing()

    def load(self):
        """
        Load a stored game

        :return: None
        """
        if len(self._unfinished_games) != 0:
            # Show a window with all stored games and a chooser
            self._load_window = tk.Tk()
            self._load_window.geometry("250x150")
            self._load_window.resizable(False, False)
            self._load_window.title("Select a game")
            self.var = tk.StringVar(self._load_window)
            self.var.set('Select a game')  # default option

            # Configure the window
            popup_menu = tk.OptionMenu(self._load_window, self.var, *list(self._unfinished_games))
            btn = tk.Button(self._load_window)

            popup_menu.pack(expand=True)
            popup_menu.configure(font=(self._fontLight, 14), relief=tk.FLAT)

            btn.pack(side=tk.BOTTOM, pady=10)
            btn.configure(text='Done', font=(self._fontLight, 14))
            btn.configure(command=self.buttonfn)
            btn.configure(bg=self._colorAccent, fg="white", relief=tk.FLAT)

    def buttonfn(self):
        """
        Load a stored game with the game choosed

        :return: None
        """
        self.start_game(self._unfinished_games[self.var.get()])
        self._load_window.destroy()

    def search_games(self):
        """
        Search the file and the stored games. If the file doesn't exist, creates a new one

        :return: None
        """

        # Serach the file
        if os.path.isfile(self.path):
            file = open(self.path, 'rb')
            self._store_dict = pickle.load(file)
            self._unfinished_games = self._store_dict["unfinished"]
            self._win_games = self._store_dict["win"]

        # Create new file
        else:
            file = open(self.path, 'wb')
            pickle.dump(self._store_dict, file)

        file.close()

    def give_up(self):
        """
        Close the actual game, asking for store it and start a new game

        :return: None
        """

        # Ask for store the game
        if self._game is not None:
            if tk.messagebox.askyesno("Question", "Do you want to save before give up?"):
                self.save()

            # Start a game
            self.start_game()

    def on_closing(self):
        """
        Listener who ask the player to save the current game and close the game

        :return: None
        """

        # Ask for save the game
        if self._game is not None:
            if tk.messagebox.askokcancel("Quit", "Do you want to save before exit?"):
                self.save()

        # Close the application
        self.parent.destroy()

    def board_update(self):
        """
        Resize de board

        :return: None
        """
        self.board.configure(width=self.board.winfo_height())

    def solve(self):
        """
        The IA resolve the game and show the steps every 0.5 seconds

        :return: None
        """
        board_to_solve = self._game.tiles.get_numbers_from_tiles()
        path = AstarSolver(board_to_solve, self.gen_target_board(""), "").solve()
        print(path)
        self._game.keysPressed = []
        for board_state in path:
            self.update_ia_move(board_state)

    def gen_target_board(self, gap):
        """
        Genera el tablero objetivo
        :param gap: gap
        :return: tablero
        """
        alist = []
        dim = self.grid.get()
        for i in range(dim * dim):
            if i == dim * dim - 1:
                alist.append(gap)
            else:
                alist.append(i + 1)

        return alist

    def update_ia_move(self, board_state):
        """
        Actualiza la interfaz tras un movimiento de la ia
        :param board_state:
        :return: None
        """
        print("To load: ", board_state)
        self._game.tiles.load(board_state)
        self._game.tiles.update_tiles()
        self._game.keysPressed.append("IA")
        self.labelNumMov.configure(text=len(self._game.keysPressed))
        self.parent.update()
        time.sleep(self._refresh_time)
